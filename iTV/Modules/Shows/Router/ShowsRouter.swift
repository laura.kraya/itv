import UIKit

class ShowsRouter {
    
    static func start() -> UIViewController {
        
        let presenter = ShowsPresenter(router: ShowsRouter())
        
        let controller = ShowsViewController()
        
        return controller
    }
}
