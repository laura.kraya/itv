//
//  ShowsViewController.swift
//  iTV
//
//  Created by Laura Daniela Krayacich on 29/01/2020.
//  Copyright © 2020 Laura Daniela Krayacich. All rights reserved.
//

import UIKit

class ShowsViewController: UIViewController {
    
    @IBOutlet weak var responseTextView: UITextField!
    
    private let restClient = RestClient()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    @IBAction func showsRequestButton(_ sender: Any) {
        
        guard let urlToExecute = URL(string: "http://api.tvmaze.com/shows") else {
            return
        }
        
        restClient.execute(urlToExecute) { (json, error) in
            if let error = error {
                self.responseTextView.text = error.localizedDescription
            } else if let json = json {
                self.responseTextView.text = json.description
            }
        }
        
    }
    
}
