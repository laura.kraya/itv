import UIKit

protocol ShowsPresentable: UIViewController {
    
}

class ShowsPresenter {
    
    private let interactor = ShowsInteractor()
    
    private let router: ShowsRouter
    
    weak var view: ShowsPresentable?

    
    init(router: ShowsRouter) {
        self.router = router
        interactor.delegate = self
        print(self)
    }
}

extension ShowsPresenter: ShowsInteractorDelegate {
    
}
