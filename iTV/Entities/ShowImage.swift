import Foundation

struct ShowImage: Codable {
    let medium: String
    let original: String
    
    
    private enum CodingKeys: String, CodingKey {
        case medium = "medium"
        case original = "original"
    }
    
}
