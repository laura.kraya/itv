import Foundation

struct Episode: Codable {
    let name: String
    let number: Int8
    let season: Int8
    
    private enum CodingKeys: String, CodingKey {
        case name = "name"
        case number = "number"
        case season = "season"
    }
}
