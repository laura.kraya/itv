import Foundation

/*
Una primera pantalla wue nos traiga las shows (name, rating), y que podamos ir al detalle, en el detalle que nos muestre determinada name, summary, image y una lista de episodios.
*/

struct Show: Codable {
    let id: Int
    let name: String
    let rating: ShowRating
    let summary: String
    let image: ShowImage
    
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case rating = "rating"
        case summary = "summary"
        case image = "image"
    }
    
}
