import Foundation

struct ShowRating: Codable {
    let average: Float
    
    private enum CodingKeys: String, CodingKey {
        case average = "average"
    }
    
}
