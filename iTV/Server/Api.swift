enum Server: String {
    
    case production = "http://api.tvmaze.com"
    case testing = "<url a testing>"
}

enum EndPoint: String {
    
    case getAllShows = "/shows"
    case getShowDetail = "/show/getShowDetail"
    case getShowEpisodeList = "/show/getShowDetail/getShowEpisodeList"

}

class Api {
    
    private let server: Server
    
    //Constructor
    required init(server: Server) {
        self.server = server
    }
    
    //var client: RestClient { return RestClient(host: server.rawValue) }

    class func production() -> Api {
        return Api(server: .production)
    }
    
    class func testing() -> Api {
        return Api(server: .testing)
    }
}
